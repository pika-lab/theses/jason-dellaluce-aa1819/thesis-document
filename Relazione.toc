\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{\numberline {1}Introduzione}{3}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Background}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Programmazione logica}{6}{section.2.1}% 
\contentsline {section}{\numberline {2.2}tuProlog}{7}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Context-Awareness nelle tecniche di Reasoning}{9}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Logic Programming as a Service}{10}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Machine Learning}{12}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Alberi decisionali}{14}{section.2.6}% 
\contentsline {chapter}{\numberline {3}Valutazione di alcuni framework di Machine Learning}{17}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Anaconda}{17}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Keras e TensorFlow}{20}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Weka}{22}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Analisi conclusiva}{25}{section.3.4}% 
\contentsline {chapter}{\numberline {4}Explainable AI}{27}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Formalizzazione delle strutture}{28}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Modellazione di un'architettura}{30}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Struttura concettuale}{30}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Machine Learning Interface}{31}{subsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.2.3}Knowledge Base}{32}{subsection.4.2.3}% 
\contentsline {subsection}{\numberline {4.2.4}Machine Learning to Prolog}{33}{subsection.4.2.4}% 
\contentsline {subsection}{\numberline {4.2.5}Prolog to Machine Learning}{35}{subsection.4.2.5}% 
\contentsline {subsection}{\numberline {4.2.6}Demonstration}{36}{subsection.4.2.6}% 
\contentsline {section}{\numberline {4.3}Considerazioni finali}{37}{section.4.3}% 
\contentsline {chapter}{\numberline {5}Realizzazione di un prototipo}{40}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Scelte architetturali}{40}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Struttura logica dell'applicativo}{42}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Implementazione dei modelli spiegabili}{50}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Soluzione con interfaccia Drawable}{53}{subsection.5.3.1}% 
\contentsline {subsection}{\numberline {5.3.2}Integrazione algoritmo J48}{55}{subsection.5.3.2}% 
\contentsline {subsection}{\numberline {5.3.3}Integrazione algoritmo REPTree}{56}{subsection.5.3.3}% 
\contentsline {subsection}{\numberline {5.3.4}Integrazione algoritmo RandomTree}{58}{subsection.5.3.4}% 
\contentsline {section}{\numberline {5.4}Implementazione dei modelli di dimostrazione}{58}{section.5.4}% 
\contentsline {subsection}{\numberline {5.4.1}Integrazione dimostratori in linguaggio naturale}{61}{subsection.5.4.1}% 
\contentsline {section}{\numberline {5.5}Implementazione dei modelli di persistenza}{63}{section.5.5}% 
\contentsline {section}{\numberline {5.6}Collaudo}{65}{section.5.6}% 
\contentsline {subsection}{\numberline {5.6.1}Spiegazione con un problema classificativo}{65}{subsection.5.6.1}% 
\contentsline {subsection}{\numberline {5.6.2}Spiegazione con un problema regressivo}{69}{subsection.5.6.2}% 
\contentsline {subsection}{\numberline {5.6.3}Dimostrazione con una Knowledge Base generica}{72}{subsection.5.6.3}% 
\contentsline {subsection}{\numberline {5.6.4}Dimostrazione con un modello spiegato}{75}{subsection.5.6.4}% 
\contentsline {chapter}{\numberline {6}Considerazioni e sviluppi futuri}{78}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Considerazioni sulla proposta architetturale}{79}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Considerazioni sul prototipo}{81}{section.6.2}% 
\contentsline {chapter}{\numberline {7}Conclusioni}{85}{chapter.7}% 
