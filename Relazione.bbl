\begin{thebibliography}{10}

\bibitem{anaconda-page}
{A}naconda {H}ome {P}age.
\newblock \\ \url{https://www.anaconda.com/}.

\bibitem{google-soc-page}
{G}oogle {S}ummer of {C}ode.
\newblock \\ \url{https://summerofcode.withgoogle.com/}.

\bibitem{apache2-page}
{A}pache {L}icense, {V}ersion 2.0.
\newblock \\ \url{https://www.apache.org/licenses/LICENSE-2.0}, 2004.

\bibitem{gnu-lgpl-page}
{GNU} {L}esser {G}eneral {P}ublic {L}icense.
\newblock \\ \url{https://www.gnu.org/licenses/lgpl-3.0.en.html}, 2007.

\bibitem{bsd-page}
{BSD} {L}icense {P}roblem.
\newblock \\ \url{https://www.gnu.org/licenses/bsd.html}, 2015.

\bibitem{theano-page}
{T}heano {D}ocumentation.
\newblock \\ \url{http://deeplearning.net/software/theano/}, 2017.

\bibitem{gnu-gpl-page}
{GNU}, {H}ome {P}age {L}icenze.
\newblock \\ \url{https://www.gnu.org/licenses/licenses.it.html}, 2018.

\bibitem{weka-javadoc}
{J}avadoc of the {Weka} {L}ibrary.
\newblock \\ \url{http://weka.sourceforge.net/doc.stable/}, 2018.

\bibitem{mit-page}
{MIT} {L}icense {P}age.
\newblock \\ \url{https://spdx.org/licenses/MIT.html}, 2018.

\bibitem{keras-page}
{Keras}: {H}ome.
\newblock \\ \url{https://keras.io/}, 2019.

\bibitem{cntk-page}
{M}icrosoft {C}ognitive {T}oolkit.
\newblock \\ \url{https://www.microsoft.com/en-us/cognitive-toolkit/}, 2019.

\bibitem{python-page}
{P}ython {S}oftware {F}oundation {P}age.
\newblock \\ \url{https://www.python.org/}, 2019.

\bibitem{tf-page}
{TensorFlow}.
\newblock \\ \url{https://www.tensorflow.org/}, 2019.

\bibitem{weka-page}
{Weka}: {D}ata {M}ining {S}oftware {I}n {J}ava.
\newblock \\ \url{https://www.cs.waikato.ac.nz/ml/weka/}, 2019.

\bibitem{book-lfdata}
Y.~S. Abu-Mustafa, M.~Magdon-Ismail, and H.~Lin.
\newblock {\em {L}earning {F}rom {D}ata. {A} {S}hort {C}ourse}.
\newblock AMLBook, 2012.

\bibitem{tuprolog-manual}
{A}lma {M}ater {S}tudiorum, {U}niversity of {B}ologna, {I}taly.
\newblock {\em {{\sf tu}Prolog Manual} for version 3.3.0}, 2018.

\bibitem{thesis-context}
Andrey Boytsov.
\newblock {\em \textit{{C}ontext {R}easoning, {C}ontext {P}rediction and
  {P}roactive {A}daptation in {P}ervasive {C}omputing {S}ystems}}.
\newblock PhD thesis, {L}uleå {U}niversity of {T}echnology, Sweden, 2011.

\bibitem{Breiman2001-article}
Leo Breiman.
\newblock Random forests.
\newblock {\em Machine Learning}, 45(1):5--32, 2001.

\bibitem{lpaas-article}
Roberta Calgari, Enrico Denti, Stefano Mariani, and Andrea Omicini.
\newblock \textit{{L}ogic programming as a service}.
\newblock 2018.

\bibitem{Cze03-article}
J.~Czerniak and H.~Zarzycki.
\newblock {Application of rough sets in the presumptive diagnosis of urinary
  system diseases}.
\newblock {\em Artifical Inteligence and Security in Computing Systems,
  ACS'2002 9th International Conference Proceedings}, pages 41--51, 2003.

\bibitem{tuprolog-article}
Enrico Denti, Andrea Omicini, and Alessandro Ricci.
\newblock \textit{{{\sf tu}Prolog}: A Light-weight {P}rolog for {I}nternet
  Applications and Infrastructures}.
\newblock 2001.

\bibitem{uci-page}
Dheeru Dua and Casey Graff.
\newblock {UCI} machine learning repository, 2017.

\bibitem{dot-manual}
Emden Gansner, Eleftherios Koutsofios, and Stephen North.
\newblock Drawing graphs with dot.
\newblock Technical report, 2006.

\bibitem{sklearn-article}
F.~Pedregosa, G.~Varoquaux, A.~Gramfort, V.~Michel, B.~Thirion, O.~Grisel,
  M.~Blondel, P.~Prettenhofer, R.~Weiss, V.~Dubourg, J.~Vanderplas, A.~Passos,
  D.~Cournapeau, M.~Brucher, M.~Perrot, and E.~Duchesnay.
\newblock Scikit-learn: Machine learning in {P}ython.
\newblock {\em Journal of Machine Learning Research}, 12:2825--2830, 2011.

\bibitem{book-Quinlan1993}
Ross Quinlan.
\newblock {\em C4.5: Programs for Machine Learning}.
\newblock Morgan Kaufmann Publishers, San Mateo, CA, 1993.

\bibitem{book-aima}
S.~Russel and P~Norvig.
\newblock {\em {A}rtificial {I}ntelligence. {A} {M}odern {A}pproach}.
\newblock Prentice Hall, 3rd edition, 2010.

\bibitem{weka-manual}
{U}niversity of {W}aikato, {N}ew {Z}ealand.
\newblock {\em {WEKA Manual} for version 3.8.3}, 2018.

\end{thebibliography}
