\chapter {Background}
\label{cap2}


\section{Programmazione logica}
Se si desidera sviluppare un agente software intelligente ed autonomo si deve garanire una capacità di pensiero
razionale, una bagaglio di conoscenza ed un relativo sistema di rappresentazione della stessa, un sistema di
interfacciamento con il mondo esterno. Per \textbf{Knowledge Base}\cite{book-aima} si intende quindi una collezione
di frasi dal carico informativo rispetto un certo aspetto della realtà, che può essere soggetta ad espansione per
aggiunta di nuove nozioni, o ad interrogazioni che ne testino la consistenza attraverso un
\textbf{sistema inferenziale} in grado di lavorare sui concetti noti.
La logica è il mezzo mediante il quale siamo in grado di rappresentare le componenti di una Knowledge Base e
applicare processi inferenziali sulla stessa. Una logica deve possedere una sintassi formale e non
ambigua per rappresentare le nozioni, ed una semantica che definisca il significato di una determinata frase
nei suoi possibili domini garantendo che essa mantenga sempre uno stato di verità o di falsità.

La \textbf{logica del primo ordine} è un sistema dichiarativo in grado di descrivere la conoscenza e diversi
domini della realtà ereditando il potere espressivo delle logiche proposizionali semplici, le quali
soffrono tuttavia di forti limiti nella rappresentazione del tempo e di realtà di dimensioni non finite
(Frame Problem, Logical State Estimation\cite{book-aima}).
In questa logica più evoluta tali restrizioni vengono superate accogliendo concetti tipici dei linguaggi naturali
quali la possibilità di rappresentare entità ed oggetti, proprietà e relazioni degli stessi, funzioni.
Un \textbf{termine} (Term) è un espressione logica che fa riferimento ad un oggetto. Oltre a termini costanti é
anche possibile definire \textbf{predicati} (Predicate), ossia strutture che indicano funzioni e possiedono
pertanto un nome ed un certo numero di argomenti, termini a loro volta. Il numero di argomenti di un predicato é
detto \textbf{arità} (Arity). I fatti logici possono essere rappresentati da espressioni \textbf{atomiche}
(Atom) che sono formate da simboli predicativi, con o senza argomenti.
Inoltre, la logica del primo ordine rende possibile definire regole generali in grado di accomunare oggetti variabili
e che semanticamente corrispondono ai quantificatori matematici di tipo esistenziale ($\exists$) ed universale
($\forall$).
Una \textbf{variabile} (Variable) è a tutti gli effetti un termine, convenzionalmente iniziato con una lettera maiuscola,
che indica un oggetto non costante o comunque dipendente dal dominio.

\textbf{Prolog} è un linguaggio di programmazione dichiarativo che implementa il paradigma logico.
Ideato ed implementato negli anni settanta, esso si basa sulla logica del primo ordine e la sua sintassi é
costituita da formule dette \textbf{clausole di Horn}, ossia disgiunzioni di letterati del primo ordine.
La notazione usata è particolarmente intuitiva ed ideata per essere compresa anche da utenti distanti
dal settore dell'informatica.
Sono rappresentabili atomi, strighe, numeri (interi e reali), variabili, termini composti, liste, regole
generiche.
L'esecuzione di un programma Prolog corrisponde alla dimostrazione di un teorema per risoluzione
(Proof by Resolution\cite{book-aima}) che si basa sui concetti di unificazione, ricorsione, e backtracking.

Queste caratteristiche sono comuni negli approcci programmativi \textbf{dichiarativi}, che prevedono
la descrizione di un problema in un determinato dominio mediante le entità e le relazioni che lo costituiscono,
senza però mai specificare il modo in cui le attività sugli stessi debbano essere svolte.
Questo è in opposizione con il paradigma \textbf{procedurale}, alla base della maggior parte dei linguaggi di
programmazione odierni, che prevede la codifica del comportamento desiderato dal software sotto forma di
procedure ed algorimi che vanno eseguite in sequenza e sempre nello stesso modo.
Per quanto la validità dell'uno rispetto all'altro sia stata nel tempo molto discussa, è ad oggi noto
che un agente intelligente ideale deve preferibilmente godere delle qualità di entrambi.
Sarebbe quindi desiderabile possedere un'infrastruttura programmativa in grado di concigliare opportunamente
i due approcci.



\section{tuProlog}
tuProlog\cite{tuprolog-article} è un framework Prolog sviluppato nell'Università di Bologna,
scritto in Java, adatto ad applicazioni ed infrastrutture distribuite, open-source e disponibile
sotto licenza LGPL Lesser General Public License \cite{gnu-lgpl-page}.
Esso è progettato secondo criteri di minimalità, configurabilità dinamica, e stretta
integrazione con l'infrastruttura Java in modo da supportare e favorire la programmazione
multi-paradigma. È quindi possibile sfruttare paradigmi di programmazione dichiarativi
(Prolog), imperativi (Java), e ibridi ottenendo ampie frontiere nello sviluppo di
agenti intelligenti.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.2\textwidth] {Images/LogoTuProlog}
	\caption{Logo del software tuProlog}
	\label{tuprolog-logo}
\end{figure}

La forte configurabilità si mostra nella possibilità di personalizzazione statica
e dinamica del motore Prolog con librerie aggiuntive, cioé pacchetti di predicati,
funtori ed operatori. Diversi pacchetti standard sono inclusi di default nel sistema,
ma l'utilizzatore può facilmente crearne di propri in linguaggio Prolog, Java o in entrambi.
Inoltre, il rappresentare la Prolog Virtual Machine come oggetto Java rende possibile
istanziare un numero arbitrario di motori logici configurabili in maniera indipendente per
ogni bisogno specifico, rendendo questo sistema adatto allo sviluppo di componenti e
microservizi di applicazioni distribuite.

Nei capitoli successivi verrà fatto riferimento ad alcune classi Java appartenenti a questo software,
che sono pertanto riassunte nel sottostante elenco. Costituendo queste solo una frazione delle componenti
del progetto, per ulteriori approfondimenti si faccia riferimento al manuale ufficiale\cite{tuprolog-manual}.

\begin{description}
	\item[Term] | Questa classe è la radice della sua gerarchia, e fornisce l'interfaccia
	base che ogni termine Prolog deve rispettare secondo i canoni del design-pattern Composite.
	Le quantità numeriche sono specificazioni della classe figlia \textbf{Number}, le variabili Prolog sono rappresentate dalla classe figlia \textbf{Var}, mentre predicati, strutture composte e liste sono costruibili con la classe \textbf{Struct}.
	Si rende quindi possibile rappresentare predicati e teorie complesse mediante annidamento delle
	precedenti strutture avendo così un controllo più profondo sulle istanze dei singoli
	termini, tuttavia si rivela spesso più opportuno utilizzare il parser tuProlog mediante il
	metodo statico \texttt{createTerm} che processa stringhe o più in generale \textbf{InputStream}.
	
	\item[Theory] | Questa classe rappresenta una generica teoria Prolog. Essa può essere
	costruita a partire da una rappresentazione testuale proveniente da un qualsiasi
	\textbf{InputStream} o da istanze di Term, e può essere salvata su
	\textbf{OutputStream}.	
	Si rende possibile costurire teorie anche in maniera intrementale tramite il metodo
	\texttt{append}.	
	Una teoria è esplorabile in tutti i suoi termini contenuti usando la semantica della classe Java
	\textbf{Iterator}.
	
	\item[Prolog] | Questa classe rappresenta la macchina virtuale Prolog stessa.
	Tramite questa interfaccia minimale è possibile gestire le teorie utili alle dimostrazioni,
	configurare le librerie e ottenere risposte ad interrogazioni mediante il metodo \texttt{solve}.
	É anche possibile iterare tra tutte le soluzioni alternative mediante i metodi
	\texttt{hasOpenAlternatives} e \texttt{solveNext}.
	Inoltre, tramite l'ascolto di eventi di vario genere, è anche possibile a livello applicativo
	entrare in contatto con lo stato interno del motore logico durante i processi risolutivi.	
\end{description}



\section{Context-Awareness nelle tecniche di Reasoning}
Con Context-Awareness si indica la capacità di un sistema digitale di percepire, elaborare, adattarsi
e rispondere all'ambiente in cui è immerso. Per \textbf{contesto}\cite{thesis-context} si può intendere
un qualsiasi insieme di informazioni che possa essere utilizzata per caratterizzare la situazione di una
certa entità.
Nonostante il concetto sia di natura interdisciplinare esso è fortemente caratterizzante dei sistemi
pervasivi informatici, la cui funzionalità principale è proprio l'adattamento all'ambiente nel quale
le componenti sono immerse.
Un sistema è detto \textbf{Context-Aware}\cite{thesis-context} quando è in grado di sfruttare le
informazioni contestuali vantaggiosamente per adattarsi ad esse ed offrire agli utenti informazioni
e servizi pertinenti, la cui rilevanza dipenderà dalle attività degli utenti stessi.

Nei sistemi software pervasivi le informazioni sul contesto sono tipicamente viste come un flusso di dati
derivanti da sensori, ossia dispositivi (fisici o virtuali) in grado di misurare direttamente le
caratteristiche dell'ambiente circostante come temperatura, luce, umidità, tempo, suoni, input
dell'utente.
I dati eterogeneri derivanti dai sensori vengono tipicamente suddivisi in informazioni contestuali di basso
livello ed alto livello. Per contesto di basso livello si intendono i dati grezzi ottenuti dai sensori che
hanno subito elaborazione minima o nulla, portano semplicemente carico informativo nei confronti della
realtà spazio-temporale dell'ambiente, e fungono da base per l'estrazione di nozioni di alto livello.
Per contesto di alto livello si intende un insieme di informazioni astratte deducibili dai dati grezzi,
come ad esempio il comportamento dell'utente, le sue preferenze, le sue attività.
Un sistema Context-Aware deve preferibilmente lavorare su informazioni di alto livello ed essere quindi
in grado di ``dare significato ai dati'' dal punto di vista della semantica dell'applicazione.

Nel caso dei sistemi intelligenti dotati di capacità di ragionamento logico il concetto di Context-Awareness
può essere visto come la capacità di un agente di riconoscere l'ambiente circostante ed elaborare
soluzioni adeguate e coerenti allo stesso.
Un esempio applicativo potrebbe essere quello di un agente autonomo il cui compito è quello di uscire da un labirinto
(Wumpus World\cite{book-aima}). Per raggiungere il suo scopo risulterebbe in ogni istante ad esso necessario
conoscere la propria posizione attuale, la locazione ed il moto di eventuali insidie, la distanza dall'uscita
e dal muro più vicino, la disposizione degli ostacoli circostanti.
In tal caso la Knowledge Base codificata a priori funge come mezzo per la comprensione dei dati
contestuali derivanti dall'ambiente, veri protagonisti del problema.

In altre parole, un sistema intelligente logico cosciente del contesto circostante può alterare le proprie
computazioni al variare dell'ambiente fisico-computazionale in cui è immerso.



\section{Logic Programming as a Service}
I sistemi di computazione sono ad oggi spesso direzionati verso ambienti pervasivi ed ubicati in cui i
dispositivi, gli agenti software, ed i servizi necessitano di integrarsi e cooperare con gli utenti umani
per soddisfarne le richieste ed anticiparne i bisogni\cite{lpaas-article}.
Questo scenario necessita di soluzioni software intelligenti che sfruttino la conoscenza del dominio,
comprendano il contesto locale, e possano servire da base per applicazioni e servizi intelligenti.

\textbf{Logic Programming as a Service} (LPaaS)\cite{lpaas-article} è un approccio innovativo alla necessità
di sistemi pervasivi intelligenti che propone una rivisitazione del concetto di Programmazione Logica sotto
forma di servizio distribuito.
Proprio per la necessità di facile distribuzione, l'attuale prototipo implementativo del progetto utilizza
\textbf{tuProlog} come motore logico interno.
LPaaS è vista come la naturale evoluzione della Programmazione Logica distribuita nei sistemi pervasivi,
esplicitamente progettata per sfruttare la Context-Awareness e promuovere l'intelligenza situata e
distribuita negli Smart Environments. A tal fine, sono stati reinterpretati diversi aspetti della
Programmazione Logica classicamente conosciuta secondo i principi di \textbf{incapsulamento},
\textbf{statelessness}, e \textbf{località} considerando le tre dimensioni di spazio, tempo, e contesto.

Le interazioni \textit{stateless} sono un primo forte cambiamento. Normalmente infatti un motore logico
sfrutta la teoria fornita dall'utente come Knowledge Base e la mantiene come stato interno insieme
alle interrogazioni e ai passi delle relative dimostrazioni, in modo da rendere il tutto esplorabile
dall'utilizzatore.
Rendendo le interazioni con l'utente autocontenute si può invece offrire un servizio distribuito
più ridondante e disponibile in cui diverse istanze di motori logici possono rispondere alle richieste
in maniera intercambiabile.
I vantaggi delle interazioni stateful possono essere raggiunti con adeguate soluzioni software middleware
del servizio.

Anche il principio di località è innovativo e non presente negli approcci classici della Programmazione
Logica, nei quali il motore logico è visto come una componente monolitica le cui computazioni rimangono
invariate a parità di interrogazioni e stato iniziale, ed in cui le teorie sono sempre considerate vere
anche in diverse circostanze temporali.
Secondo questo nuovo approccio la computazione potrebbe invece essere liberamente influenzata dalla macchina
ed il contesto di esecuzione in cui il motore logico è rilocato, ed al contempo determinati processi
risolutivi potrebbero essere validi solo in relazione ad un relativo \textit{timestamp} definito dall'utente.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\textwidth] {Images/LPaaSInterface}
	\caption{Interfacce utente dell'architettura LPaaS}
	\label{lpaas-interface}
\end{figure}

L'incapsulamento è invece uno strumento efficace atto ad astrarre l'utente dai dettagli interni del servizio,
pur mantenendo disponibili sistemi di interazione adeguati per l'osservazione e l'utilizzo.
Come mostrato nella Figura \ref{lpaas-interface}, LPaaS prevede due diverse interfacce utente che separano i
privilegi del cliente semplice da quelli di un amministratore capace di manipolare la configurazione del servizio.

Si intende qui mettere in luce come questa soluzione possa integrasi armonicamente con
le moderne tecniche di apprendimento automatico, le quali possono fornire un mezzo coerente per la creazione
di Knowledge Base dinamiche dipendenti da spazio, tempo, e contesto. Questo favorirebbe la creazione di servizi
distribuiti Context-Aware in grado anche di avere comportamenti proattivi nei confronti degli utenti,
apprendendo le loro necessità attraverso l'interazione diretta con gli stessi.



\section {Machine Learning}
Dato un determinato fenomeno o meccanismo siamo frequentemente interessati ad una conoscenza
profonda dello stesso che ci permetta di decriverne formalmente il funzionamento.
Quando questo si rivela possibile siamo in grado di sviluppare sistemi autonomi capaci
di interpretare le manifestazioni passate di tale fenomeno e di prevedere con affidabilità
il modo in cui questo si evolverà in futuro.
Nella maggioranza dei casi, tuttavia, non siamo in grado di raggiungere un livello di conoscenza
adeguato del sistema in esame.

Il termine \textbf{Machine Learning}\cite{book-lfdata} identifica l'insieme di tecniche e metodi che
mediante l'esaminazione di considerevoli quantità di dati tentano di riconoscere pattern ricorrenti negli
stessi, apprenderne associazioni, e predire con precisione accettabile il modo in cui nuove istanze della
stesso genere possano manifestarsi. 
In questo modo siamo in grado di sviluppare applicazioni automatiche anche nel caso in cui programmare
esplicitamente algoritmi risolutivi per una certa problematica risulterebbe impraticabile.
Generalmente è previsto l'impiego di un determinato algoritmo che riesca a plasmare una struttura
dati nota attraverso la valutazione di un grande flusso di dati in entrata provenienti da un campione.
La collezione degli esempi di dati che si ha a disposizione per questa analisi viene denominata \textbf{Data Set},
e i tipi di dato che costituiscono ogni singola istanza si dicono \textbf{Attributi} o \textbf{Features}.
La complessità ed i tempi di computazione di questo processo sono fattori determinanti nella scelta
del modello corretto per un'applicazione, e principali oggetti di studio della branca
dell'informatica chiamata \textbf{teoria dell'apprendimento}.

In base ai metodi, alla forma dei dati, ai risultati desiderati, queste metodologie vengono
tipicamente suddivise in tre macrocategorie\cite{book-lfdata}:
\begin{itemize}
	\item \textbf{Apprendimento supervisionato} | In questo caso ogni istanza dei dati raccolti
	include un insieme di attributi di input ed uno o più output.
	Lo scopo è riuscire ad apprendere una regola in grado di associare tutti gli attributi in
	input agli output ricercati, in modo da poter predire questi ultimi in casi futuri.
	Le problematiche di questo genere si dicono di tipo \textbf{classificativo} quando si è
	interessati a riconoscere esiti rappresentabili in una classe finita e discreta di valori,
	o di tipo \textbf{regressivo} nel caso in cui lo scopo sia riprodurre in output una quantità
	numerica di tipo continuo.
	
	\item \textbf{Apprendimento non supervisionato} | A differenza di quanto appena citato, questi
	metodi sono atti a cercare associazioni interne tra gli attributi stessi in input, escludendo
	quindi gli output dall’analisi (di qui la “mancanza di supervisione” nel nome).
	Questo è particolarmente utile per riconoscere pattern di vicinanza o similitudine in
	dati con numerose dimensioni o attributi, che sarebbero altrimenti complessi e costosi da
	ricercare manualmente.
	
	\item \textbf{Apprendimento per rinforzo} | In questo caso l’algoritmo è interessato a
	raggiungere un obiettivo o riprodurre un output senza però avere esempi che associno
	gli attributi in input con l’esito desiderato.
	La macchina riesce a raggiungere il suo scopo attraverso numerosi tentativi e relativi
	feedback che la guidano a raggiungere un comportamento quanto più performante nei confronti
	dell’obiettivo richiesto. Con queste metodologie, che mimano il nostro modo empirico
	di apprendimento, si possono raggiungere obiettivi complessi tra i quali il movimento di arti meccanici, guidare automobili o competere nei videogiochi.
\end{itemize} 

É necessario tenere in considerazione che qualunque sia la quantità di dati disponibili per
l'addestramento di un modello di apprendimento, questi costituiranno sempre e comunque un campione
limitato dell'insieme di tutti gli esempi possibili. Viene a proposito definito \textbf{In-Error}
l’errore commesso nelle previsioni sul campione di dati disponibile, e \textbf{Out-Error}
l’imprecisione che si prospetta di avere sul resto della popolazione di dati sconosciuta a priori.
In questo senso, si intende per \textbf{generalizzazione}\cite{book-lfdata} l'abilità di un modello
di adeguarsi correttamente ad istanze di dati mai viste in precedenza durante il processo di
apprendimento. Affinché la generalizzazione offra prestazioni ideali, la complessità del modello
costruito deve essere simile a quella del fenomeno osservato, in caso contrario si potrebbe incorrere
in \textbf{Underfitting} quando l'approccio scelto risulta essere troppo semplicistico, o in
\textbf{Overfitting} se l'adeguamento ai dati del campione è particolarmente forte e si
manifesta scarsa capacità di adeguamento ad esempi nuovi. Questi fenomeni si rilevano più
frequentemente all'aumentare del numero di attributi, e vanno in diminuendo al crescere
del numero di esempi dell'allenamento.

Oltre alla limitatezza intrinseca dell'algoritmo scelto, un altro ostacolo per una corretta
generalizzazione del modello di apprendimento potrebbe essere costituito dalla morfologia stessa
dei dati. Infatti, all'interno dei campioni di esempi disponibili potrebbero essere presenti errori,
imprecisioni, incertezze che possono essere dovute ad errori di misura, trascrizione, o semplicemente
dall'aleatorietà naturale del fenomeno osservato.
Proprio per questa ragione sono tipicamente penalizzati modelli troppo complessi, che per quanto
si dimostrino performanti in fase di addestramento potrebbero risultare inadeguati ad applicazioni
reali.
Per concludere, la scelta della tecnica di apprendimento più prestante per un determinato caso di
studio è quell'attività che viene definita \textbf{Model Selection}.



\section {Alberi decisionali}

L'apprendimento di alberi decisionali è una delle tecniche di Machine Learning di maggior successo, pur
essendo al contempo una delle più semplici.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth] {Images/DecisionTreeExample}
	\caption {Esempio grafico di albero decisionale}
	\label{decision-tree-example}
\end{figure}
Un albero decisionale rappresenta una funzione in grado di restituire un singolo valore in uscita 
(anche detto ``decisione'') a partire da un'istanza di dati proveniente da un Data Set\cite{book-aima}.
I valori degli attributi in entrata ed in uscita possono essere sia di natura continua che discreta. 
Queste strutture ad albero prevedono che per ogni nodo sia presente un determinato test su un attributo
dell'istanza considerata, che ogni arco rapprenti uno dei valori possibili di tale attributo, e che i nodi
foglia contengano il valore della classe ricercata.
Ad un'istanza di dati viene quindi associata una determinata decisione se e solo
se tutti i test di comparazione sui suoi attributi vengono soddisfatti. Ciò comporta che ogni albero
decisionale possa quindi essere naturalmente rappresentato come disgiunzione di predicati logici congiunti
(DNF\cite{book-aima}, Disjunctive Normal Form), godendo pertanto di massima comprensibilità dei propri
criteri interni da parte degli esseri umani.

La costruzione di un albero corretto per una certa applicazione può tuttavia dimostrarsi un processo
computazionalmente molto costoso. Le tecniche di apprendimento di queste strutture prevedono tipicamente
la ricerca di un attributo ottimale in grado classificare il maggior numero di esempi del campione di dati.
Iterando questo principio su tutti gli attributi disponibili e tentando di dividere i loro valori possibili
si costruiscono in maniera incrementale tutti i percorsi dell'albero.
Per quanto riguarda gli attributi in input di tipo numerico, non essendoci la possibilità di eseguire test
su una quantità finita di valori, si ricerca tipicamente uno \textbf{split-point} che divida l'intervallo
numerico ricoducendosi ad un caso di scelta binaria.
Si rivela quindi in genere necessario misurare il \textbf{guadagno informativo} di un certo attributo.
Questa grandezza è quantificata in termini di \textbf{entropia}\cite{book-aima}, che è definita come misura
dell'incertezza di una variabile aleatoria.

Formalmente si definisce l'entropia di una variabile casuale $V$ con $v_{k}$ valori, ognuno di
probabilità $P(v_{k})$, come:
\begin{center}
	$H(V) = \sum_{k}^{} P(v_{k}) \cdot log_{2}\frac{1}{P(v_{k})} $
\end{center}
Secondo questa relazione si deduce che l'entropia di una variabile casuale Booleana, con probabilità $q$ di
risultare vera, può essere definita come:
\begin{center}
	$B(q) = -(q \cdot log_{2}q + (1 - q) \cdot log_{2}(1 - q) )$
\end{center}
Tornando agli alberi decisionali, se il campione di dati d'allenamento contiene $p$ esempi positivi e $n$
esempi negativi si può pensare l'entropia dell'attributo oggetto di classificazione come:
\begin{center}
	$H(AttributoClasse) = B(\frac{p}{p+n})$
\end{center}
Durante il processo di apprendimento si è interessati a conoscere il grado di influenza informativa di
un certo attributo. Conoscendo l'entropia della struttura ad albero prima dell'inserimento
di un test sull'attributo in esame, e definendo $Rimanenza(Attributo)$ l'entropia risultante dopo la
sua aggiunta nel modello classificativo, arriviamo a definire il \textbf{guadagno informativo} come:
\begin{center}
	$Guadagno(Attributo) = B(\frac{p}{p+n}) - Rimanenza(Attributo)$
\end{center}
Avendo quindi una misura dell'importanza di un attributo, si possono scartare a priori un buon numero di
ipotesi durante il processo di apprendimento dell'albero decisionale, rendendo il tutto computazionalmente
più efficiente.

La presenza di incertezza e rumore nei dati di allenamento può portare alla creazione di alberi più complessi
di quanto necessario e in generale al fenomeno di Overfitting.
Per compensare queste problematiche negli alberi decisionali si applica la tecnica chiamata
\textbf{pruning}\cite{book-aima}, che prevede l'utilizzo delle nozioni di guadagno informativo ed entropia
per eliminare i nodi che portano a decisioni non particolarmente significative.

Gli alberi decisionali rispondono intuitivamente ad indagini di tipo classificativo, tuttavia essi sono
estensivamente utilizzati anche per applicazioni di tipo regressivo nel cui caso i nodi foglia contengono un
valore numerico continuo caratteristico della decisione.
In base all'implementazione del modello tale valore può essere costante, provenire da una funzione lineare,
o rappresentare un sistema probabilistico.

Concludendo, la forte comprensibilità dei criteri decisionali del modello e la conseguente possibilità di
spiegazione di un suo output sono proprietà molto importanti per l'utilizzo in applicazioni reali, che
potrebbero avere anche valenza legale in base all'importanza dell'informazione ottenuta. La maggior parte
degli altri modelli di apprendimento ad oggi noti non gode di questo tipo di vantaggio.



