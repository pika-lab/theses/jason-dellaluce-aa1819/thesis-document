\chapter {Explainable AI}

Possedere algoritmi e modelli in grado di apprendere autonomamente il funzionamento di un fenomeno, o una struttura
che vi somigli con una certa confidenza, è ormai un'opportunità irrinunciabile in tutti gli ambienti lavorativi.
Scienziati, ricercatori, aziende, medie e piccole attività sono sempre più interessate ad adottare strumenti
automatici per eseguire previsioni o trarre conclusioni sui dati raccolti nei loro servizi, in modo da avere risposte
mirate e quanto più appropriate alle loro esigenze. Tuttavia ci si interroga sempre più frequentemente su quanto le
previsioni sentenziate da un ``intelligenza'' non umana possano essere attendibili e, anche nel caso in cui la correttezza
possa esserne matematicamente accertata, si dimostra comunque desiderabile anche comprendere il processo decisionale che ha
portato ad un dato tipo di risposta. Purtroppo però i modelli di apprendimento più efficaci si rivelano essere quelli più
complessi. Reti Neurali Artificiali, modelli non parametrici, sistemi di ensembling e moltre altre strutture vengono spesso
identificate con il termine \textbf{Black Box} proprio per via del loro funzionamento interno inscrutabile.
Vi è quindi un compromesso limitativo tra la complessità intrinseca del modello e l'accuratezza delle sue previsioni.
Sfortunatamente, l'interpretabilità delle risposte e dei processi risolutivi è in molti ambiti un fattore cruciale che
può compromettere l'adozione di massa, l'utilizzo industriale, la documentazione, lo studio, la supervisione, l'accettazione
umana e la fiducia.
In questo senso, si rivela necessario dare significato intelligibile sia alle risposte in uscita da un modello di apprendimento che alla struttura interna che è stata in grado di produrle.
Si pensi, per esempio, ad un medico che intende servirsi di una struttura predittiva addestrata per diagnosticare ai pazienti
una certa patologia: per operare o prescrivere opportune cure, egli necessita di strumenti adeguati che diano umana
leggibilità nei termini specifici del caso d'uso alle classificazioni ottenute, e di poter comprendere ed analizzare
tutti i fattori anamnestici che possono averle determinate.

Il termine \textbf{Explainable AI} si riferisce quindi alle tecniche di Intelligenza Artificiale che possono essere
agevolmente comprese dalle persone, sollevando la necessità di sistemi che favoriscano la spiegabilità e
l'interpretabilità delle strutture tecniche coinvolte ambendo ad un rapporto umano-macchina
basato sulla fiducia.
Questa tesi, nella sua natura sperimentale, si propone di formalizzare una possibile soluzione al problema della spiegabilità
dei modelli di apprendimento, introducendo e modellando un'infrastruttura software che permetta la creazione di agenti
intelligenti capaci di imparare nozioni dall'ambiente circostante e di elaborare inferenze umanamente comprensibili,
mantenendo il tutto adattabile ed estendibile a domini multipli.
Di tale infrastruttura verrà anche realizzato un prototipo, che sarà dettagliatamente discusso nel capitolo successivo.



\section {Formalizzazione delle strutture}

Come reso noto nella prefazione del capitolo l'adozione di modelli di apprendimento facilmente interpretabili comporta, nella maggior parte dei casi, un potenziale inferiore nei termini della correttezza delle loro previsioni.
Obiettivo fondamentale dell'ambito di ricerca Explainable AI è perciò quello di estrarre spiegazioni comprensibili ed intelligibili dagli attuali modelli di previsione, senza però sostituirli o modificarli.
Tale risultato è raggiungibile attraverso l'applicazione di diverse tecniche eterogenee che costituiscono un processo generalmente identificato con il termine \textit{Explanator}.
Lo stato dell'arte riconosce al momento due diverse categorie di Explanator. I primi, detti \textit{locali}, tentano di ricostruire una spiegazione per ogni previsione particolare di un determinato modello di apprendimento, mentre i secondi, chiamati \textit{globali}, puntano invece ad una spiegazione dei modelli stessi nella loro interezza.

Nonostante le diverse proposte per il raggiungimento tali di risultati, esistono numerose domande che non trovano ancora una risposta certa.
Tra i problemi fondamentali ancora irrisolti vi è ad esempio la mancanza di un comune accordo su cosa una ``spiegazione'' effettivamente sia. A tal proposito, sviluppi recenti hanno delineato alcune proprietà fondamentali che un costrutto spiegabile dovrebbe garantire, tra cui correttezza, completezza, compattezza.
Partendo da tali presupposti questa tesi propone come possibile risposta l'integrazione funzionale di approcci sub-simbolici e simbolici, mirando alla codifica delle strutture dei modelli di previsione in collezioni di predicati di logici.
Essendo infatti la Logica del Primo Ordine affermata e conosciuta anche per le sue capacità descrittive della
realtà, essa rappresenta una scelta coerente a questo tipo di mansione.
La struttura decisionale di un modello appreso automaticamente potrebbe in questo modo essere codificata in una Knowledge Base la cui interrogazione corrisponderebbe semanticamente all'ottenimento di una classificazione (o una regressione) di un'istanza di dati.
Affinchè questo sia vero, è necessario garantire consistenza ed integrità dei contenuti durante il processo di trasformazione in notazione logica.

I predicati codificati per diretta corrispondenza di una struttura appresa potrebbero essere considerati di ``basso'' livello, nei termini della spiegabilità.
Un modello Machine Learning non prevede infatti, in sè, l'attribuzione di un qualsiasi significato ai dati processati, ma si limita bensì a descrivere le associazioni che legano i valori delle singole istanze alle loro relative previsioni.
Se anche nel caso migliore si disponesse di un modello in grado di apprendere direttamente regole logiche, esse
si ridurrebbero comunque ad un insieme di condizioni da verificare sugli attributi dei dati per raggiungere una classificazione.
Utilizzando a pieno il potere espressivo della Logica del Primo Ordine si potrebbero costruire invece predicati di ``alto'' livello capaci di fornire significato semantico alle nozioni logiche di basso livello nei confronti del dominio applicativo di interesse, avvicinandosi consistentemente alla comprensibilità umana.

Se si fosse in grado di rappresentare le strutture decisionali apprese automaticamente in predicati del primo ordine
allora essi potrebbero essere sfruttati nei processi risolutivi di un motore logico, il che donerebbe al sistema un valore aggiunto particolarmente significativo.
Nell'approccio classico, infatti, un sistema inferenziale logico viene configurato con una Knowledge Base preprogrammata a priori, opportunamente ampliata o modificata nei casi applicativi che lo richiedono.
Se invece nuove regole venissero generate automaticamente dalla spiegazione di un modello predittivo, si otterrebbe una Knowledge Base aggiornata e affinata in autonomia in base al flusso di dati di esempio disponibili.
Un agente intelligente dotato di queste basi di conoscenza eterogenee potrebbe apprendere nozioni nuove da spazio,
tempo, e contesto nei quali è rilocato, rendendolo a tutti gli effetti \textbf{Context-Aware} ed adattivo nei confronti dell'ambiente circostante.
Sistemi software di questo tipo sarebbero particolarmente adeguati ad un efficace interazione umano-macchina: essi potrebbero essere preprogrammati a piacimento per un determinato servizio, comprendere ed adattarsi alle abitudini dei suoi utilizzatori, ed essere in grado di dimostrare agli interessati le motivazioni che hanno determinato le loro azioni.
Questo ultimo punto prevederebbe, aggiuntivamente, soluzioni che avvicinino il linguaggio canonico della logica a modelli testuali o grafici più comprensibili alle persone, rendendo le dimostrazioni più intendibili anche agli individui più distanti dal settore dell'informatica.



\section {Modellazione di un'architettura}

La concretizzazione di quanto discusso precedentemente si traduce nell'esigenza di un'infrastruttura software in grado di
conciliare le caratteristiche dei sistemi di apprendimento automatico supervisionato e dei motori logici del primo ordine,
concependo opportune tecniche di spiegazione degli uni nei termini degli altri.
Un framework di questo genere deve mostrarsi adatto allo sviluppo di agenti intelligenti in domini multipli, mantenere sufficente
scalabilità ed integrabilità, separare concettualmente il funzionamento degli organi induttivi dell'apprendimento automatico da
quelli logici mantenendo adeguati sistemi di ponte.


\subsection {Struttura concettuale}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1.0\textwidth] {Images/ProjectEntityModel1}
	\caption{Modello ad entità della proposta progettuale}
	\label{entity-model-1}
\end{figure}

Riassuntivamente, il sistema ricercato deve rispettare i seguenti requisiti funzionali:
\begin{enumerate}
	\item \label{req-fun1} Offrire modelli di apprendimento supervisionato e relativi algoritmi in grado di costruirli
	contemplando implementazioni interne o esterne, permettendo su di essi tutti i processi lavorativi pretendibili
	in un framework di Machine Learning.
	\item \label{req-fun2} Gestione, rappresentazione, persistenza di Data Set e campioni di dati.
	\item \label{req-fun3} Dato un qualsiasi modello appreso automaticamente, ottenere una rappresentazione della sua
	struttura decisionale interna in regole logiche adatte al ragionamento simbolico.
	\item \label{req-fun4} Gestione, rappresentazione di una Knowledge Base eterogenea in grado di ospitare sia regole logiche
	programmate staticamente che ottenute dinamicamente da un modello di apprendimento.
	\item \label{req-fun5} Integrazione di un sistema Logico del Primo Ordine in grado di lavorare con tale Knowledge Base,
	contemplando implementazioni interne o esterne.
	\item \label{req-fun6} Gestione delle interrogazioni logiche effettuate dagli utenti restituendo, insieme alle soluzioni, una
	dimostrazione intelligibile del processo dimostrativo interno al motore logico.
\end{enumerate}

Questa tesi propone il modello visionabile in Figura \ref{entity-model-1} come una possibile soluzione di alto livello
a queste richieste. Nomenclatura, semantica, ed intenti di tutte le entità rappresentate saranno discussi nelle successive
sezioni.


\subsection {Machine Learning Interface}

Questa entità si occupa di soddisfare quanto esplicitato nei primi due requisiti. 
Seppur non sia una possibilità da escludere, lo sviluppo di un framework completo di Machine Learning \textit{ad-hoc}
esula dagli obiettivi principali di questo progetto. Ciò implica che questo blocco debba tentenzialmente occuparsi di fornire
un'interfaccia unificata che permetta l'integrazione di modelli di apprendimento ed algoritmi provenienti da librerie e
framework esterni. Tale interfaccia deve permettere tutte le interazioni possibili su un generico modello Machine Learning
nascondendo al mondo esterno la complessità dei sistemi incapsulati mantenendo trasparenza, minimalità, e modularità che
facilitino l'integrazione di strumenti nuovi a piacimento. Il nome proposto per questa entità è quindi sufficentemente
autoesplicativo.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\textwidth] {Images/ProjectEntityModel2}
	\caption{Flussi di lavoro previsti nel blocco Machine Learning Interface}
	\label{entity-model-2}
\end{figure}

Nella Figura \ref{entity-model-2} sono illustrati casi di d'uso e flussi di lavoro prevedibili per l'interfaccia
in questione. Oltre all'integrazione di vari modelli di apprendimento esterni l'entità deve anche occuparsi della gestione
e rappresentazione dei Data Set utilizzati nei processi di addestramento, rendendoli accessibili a tutte le implementazioni
specifiche. Si prevede quindi l'usilio di numerosi adattatori e formati standardizzati che conformino i flussi di dati in
entrata e le previsioni in uscita alle codifiche interne delle librerie supportate.

Si è inoltre intenzionati ad elaborare la struttura interna dei modelli appresi ai fini della spiegabilità.
L'entità di interfaccia é da intendersi come esclusivamente rappresentativa di un servizio che offra tutte le funzionalità tipiche
dei sistemi di Machine Learning, evitando scenari d'uso aggiuntivi.
In questo modo tale componente non é costretta agli obiettivi specifici di questo progetto, ma potrebbe essere facilmente
estesa a servizi più generici.
Come visibile in figura, è tuttavia necessario prevedere tra le uscite alcune soluzioni che codifichino la struttura sottostante
appresa per poter rendere agibili le successivi elaborazioni della stessa. Sarebbe inopportuno nei termini dell'incapsulamento
permettere ad entità esterne di interagire con le architetture sottostanti all'interfaccia, perciò è necessario che essa
sia in grado di serializzare il proprio stato (preferibilmente in formati standard) e di esporlo all'esterno.
Questo potrebbe risultare alle volte complesso in quanto tale funzionalità non è spesso una priorità deli framework di Machine
Learning disponibili sul mercato, sul quale questo sistema farebbe affidamento.


\subsection {Knowledge Base}

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth] {Images/ProjectEntityModel3}
	\caption{Modello concettuale del blocco Knowledge Base}
	\label{entity-model-3}
\end{figure}
Anche in questo caso, il nome dell'entità è sufficientemente autoesplicativo.
Lo scopo di questo blocco è infatti quello di ospitare una base di conoscienza codificata in Logica del Primo Ordine che possa essere
utilizzata a piacimento da un sistema inferenziale nei suoi processi deduttivi.
La semantica di questa entità è quindi quella di un contenitore di teorie e regole, che potrebbe essere applicabile o adattabile
a diversi domini applicativi e trovarsi alla base dei ragionamenti di un agente intelligente.
Vista la natura dell'architettura in questione, questo sistema rappresentativo dovrebbe permettere una categorizzazione di tutte
le teorie logiche contenute in modo da rendere distinguibili e tracciabili in qualsiasi momento quelle generate automaticamente dai
processi di spiagazione.
Come indicato in Figura \ref{entity-model-3}, sarebbe anche opportuno che questo blocco permetta e preveda l'interfacciamento con dei
sistemi di persistenza del proprio contenuto. Il sistema contempla infatti la possibiltà di Knowledge Base costantemente
aggiornate da sistemi di apprendimento automatico, rendendo i contenuti di questa entità soggetti a frequenti mutazioni.
Mantenendo versioni permanenti dello stato si potrebbero favorire la riutilizzabilità, la diagnostica, la correzione degli errori,
il ripristino. Si potrebbe infine essere anche interessati ad ottenere le regole ottenute dai processi di apprendimento per utilizzi
esterni a questa infrastruttura.


\subsection {Machine Learning to Prolog}

Questo blocco rappresenta gli intenti principali dell'intero progetto. A questo livello si intende infatti introdurre un'entità attiva che faccia da ponte tra i modelli di Machine Learning e le Knowledge Base dei sistemi logici.
In quanto strettamente legato all'idea di \textit{Explanator}, questo processo deve approcciare alcuni problemi fondamentali tra cui in primo luogo \textbf{interpretabilità} e \textbf{spiegabilità}.

Per interpretabilità si intende la capacità di relazionare i termini computazionali di una struttura appresa automaticamente, o di una sua rielaborazione, alla terminologia del dominio nelle quali esse sono impiegate.
Come discusso nel Capitolo \ref{cap2}, gli Alberi Decisionali costituiscono un ottimo
candidato per questo obiettivo in quanto la loro struttura classificativa ricalcalca un processo decisionale tipico della nostra
mente, che consiste nell'identificare regole di appartenenza da verificare congiuntamente per determinare una scelta. Esistono
altri modelli dalle prestazioni ottimali che riescono a mantenere trasparenza accettabile nei confronti della logica umana, dei
quali è opportuno citare \textit{Inductive Logic Programming}\cite{book-aima} in cui si tenta di riconoscere regole logiche del
primo ordine, e \textit{Reti Bayesiane}\cite{book-aima} dove l'obiettivo è ricostruire dipendenze probabilistiche tra i dati.
Purtroppo la maggior parte dei modelli ed algoritmi di apprendimento più prestanti godono di scarsissima comprensibilità
venendo utilizzati come \textbf{Black Box}, dalle quali vengono recepite risposte in uscita la cui
provenienza non è del tutto nota nemmeno ai progettisti. Tale fenomeno è dovuto al fatto che queste metodologie costruiscano
internamente relazioni matematiche nelle quali i valori dei dati sono interpretati come quantità numeriche astratte, denaturandoli
quindi del loro senso intrinseco. Interpretare strutture di questo tipo si ridurrebbe al problema di dare senso intelligibile
a funzioni numeriche a variabile multipla, che risulta generalmente impraticabile ad un automa. 
Infine, alcuni approcci alternativi prevedono un pre-processamento mirato dei dati o un utilizzo funzionalmente improprio
dei modelli non interpretabili, per forzare la costruzione di strutture che evidenzino relazioni tra i dati ottenendo quindi
sistemi ``Grey Box'' che aumentano la trasparenza delle decisioni.

Per spiegabilità si intende invece l'abilità di donare senso semantico di alto livello ad una struttura interpretabile, in modo da chiarificarne adeguatamente il significato nei casi d'uso specifici.
Affinchè ciò sia possibile è necessario avere informazioni note a priori
sulla struttura in questione e sulla morfologia dei dati da essa trattata.
Nel caso delle tecniche di Machine Learning si deve aggiuntivamente considerare che le capacità decisionali apprese
non siano sempre desiderabili ai fini delle applicazioni.
Come evidenziato nel Capitolo \ref{cap2}, i modelli di apprendimeno automatico tendono infatti a commettere errori nelle loro
previsioni o più semplicemente proporre risposte non accettabili dalle persone: per esempio in fase di apprendimento un modello
potrebbe facilmente effetturare distinzioni razziali o compiere favoritismi per ottenere le prestazioni desiderate, producendo
inavvertitamente strutture inappropriate. 
Un altro problema, indipendente dalla correttezza o dalla comprensibilità dei modelli, è quello della ``\textit{info-besity}''
nel cui caso le strutture apprese risultano particolarmente voluminose e presentano un sovraccarico informativo, sollevando
la necessità di escluderne alcune parti non utili al dominio applicativo.
La spiegabilità non comporta quindi particolari limitazioni di natura rappresentativa ma richiede una conoscenza profonda
delle circostanze di utilizzo delle strutture trattate, in modo da fornire spiegazioni di ``alto'' livello a partire dalle
regole decisionali di basso livello ottenute dall'apprendimento automatico.

Questa entità ed i suoi flussi di lavoro verranno per semplicità successivamente indicati con il termine \textbf{Explanation}.
Nella pratica, si intende interrogare il blocco Machine Learning Interface per ottenere una rappresentazione esplorabile della struttura appresa sottostante, effettuare opportune elaborazioni che portino ad una spiegazione coerente della stessa, e inserire i risultati nel blocco Knowledge Base codificandoli in predicati logici del primo ordine.
Si presume che le implementazioni di questo tipo siano multiple e dipendenti dal modelli di apprendimento e dal dominio dell'applicazione, e che sia richiesta consistentemente la supervisione di un operatore umano.
Onde evitare importanti impedimenti teorici le prime realizzazioni si potrebbero basare sui modelli facilmente intelligibili citati in precedenza, per poi considerare estensioni ed integrazioni di algoritmi più complessi una volta raggiunte metodologie di interpretazione adeguate.


\subsection {Prolog to Machine Learning}

Nelle sezioni precedenti si sono discussi benefici e limiti del processo di spiegazione di un modello di apprendimento in
predicati logici del primo ordine.
Questo blocco invece introduce l'idea di una trasformazione inversa, ossia la ricostruzione di una struttura classificativa
(o regressiva) a partire dal contenuto di una Knowledge Base.
Si pensi, per esempio, di possedere un'insieme di predicati logici ottenuti da un Albero Decisionale mediante un'opportuna
implementazione del blocco Explanation: in tal caso sarebbe ragionevole pensare che lo stesso albero possa essere rigenerato
partendo dalla base di conoscenza ottenuta. La generalizzazione di questo concetto per un qualsiasi modello di apprendimento
o per un qualsiasi insieme di regole logiche incontrerebbe tuttavia importanti ostacoli teorici, la cui discussione
non è tra gli intenti di questa tesi.
In questo processo lavorativo la struttura predittiva ottenuta sarebbe inoltre slegata da un qualsiasi algoritmo di apprendimento,
non godendo così della capacità di evolversi e migliorarsi su nuovi dati di esempio. Quest'ultima
limitazione potrebbe essere superata conformando l'output di questo blocco alla codifica precisa di un'algoritmo di Machine
Learning proveniente da un certo framework, portando tuttavia ad un'implementazione particolarmente specifica e non
riutilizzabile.

L'introduzione di questa entità all'interno dell'architettura ha quindi per ora esclusivamente scopo indicativo, mirando a mettere in luce il modo in cui essa potrebbe essere integrata nel caso se ne ideassero implementazioni valide.
Per tanto, essa non verrà ulteriormente approfondita in questo scritto.
I processi lavorativi di questo tipo verranno per semplicità successivamente indicati con il termine \textbf{Theorization}, il quale è ispirato al processo di trasformazione stesso che tratta il passaggio da una struttura logica teorizzabile ad un'entità che non lo è.


\subsection {Demonstration}

L'ultimo e più importante passo per raggiungere un'ottimale spiegabilità dei sistemi intelligenti consiste nell'agevolare
la comunicazione e l'interazione con gli stessi, così da facilitare il rapporto umano-macchina anche agli individui
meno pratici dei tecnicismi coinvolti, ambendo ad un'adozione di massa e alla distribuzione pervasiva
dell'intelligenza. In questo blocco viene quindi trattata una possibile soluzione a quanto citato nel requisito \ref{req-fun6}.
Siccome sono previsti processi lavorativi che coinvolgono la dimostrazione delle ragioni che hanno determinato le azioni di un agente
intelligente, il nome proposto per questa entità è \textbf{Demonstration}.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth] {Images/ProjectEntityModel4}
	\caption{Flussi di lavoro d'esempio per il blocco Demonstration}
	\label{entity-model-4}
\end{figure}

Si rivela perciò necessario formalizzare sia le soluzioni di una certa inferenza che i processi interni dai quali essa è stata determinata.
Mentre nel primo caso un'adeguata strategia sintattica potrebbe rivelarsi sufficiente, il secondo obiettivo richiede invece che
il motore logico in questione codifichi l'evoluzione del suo stato interno nel corso della deduzione, o che sia almeno permessa
l'osservazione dei suoi contesti esecutivi da parte di un agente esterno.
Nell'architettura proposta questa entità funge da mediatrice tra l'utente e un agente intelligente dotato di un motore Prolog,
incapsulandolo ed elaborando propriamente le sue interazioni con l'esterno, curandosi di osservare il suo stato interno per ottenere le strutture dimostrative desiderate.
L'esecuzione di un programma Prolog prevede la costruzione e l'esplorazione dell'\textbf{Albero di Prova} (o Albero di Ricerca,
Albero di Derivazione) \cite{book-aima}, che tiene traccia di tutti i passi della dimostrazione relativa ad un'interrogazione.
In questo tipo di struttura ogni nodo corrisponde ad un \textit{obiettivo} della dimostrazione, di cui la radice contiene
la query dell'utente e le foglie rappresentano predicati veri o falsi.
L'analisi dell'Albero di Prova potrebbe perciò essere una soluzione particolarmente adatta per i fini del blocco Demonstration.

Indipendentemente dal tipo di sorgente informativa scelta, è infine necessario presentare all'utente i risultati della sua
elaborazione. L'obiettivo di questa fase è la formalizzazione di strutture adeguate che permettano una facile comprensione
dei processi risolutivi agli esseri umani, utenti ultimi del sistema. Nonostante la sintassi della Logica del Primo Ordine
sia in sè sufficientemente vicina al modo in cui le persone organizzano il proprio pensiero, una lettura propria della stessa richiede
ugualmente diversi prerequisiti teorici di natura logico-matematica, di cui la maggior parte degli individui interessati al sistema
potrebbe non disporre. In Figura \ref{entity-model-4} sono mostrati alcuni esempi di rappresentazioni possibili. Tramite opportune
componenti di \textit{Natural Language Generation} si potrebbe infatti pensare alla generazione di un testo nel linguaggio naturale
dell'utente, come l'inglese o l'italiano. In alternativa potrebbero essere anche introdotte strutture grafiche di alta intelligibilità, come
per esempio dei grafi.

Il blocco di Demonstration è per tanto da intendersi come distaccato da tutti i precedenti dovendo essere in grado di favorire
interazioni user-friendly su Knowledge Base di natura generica, costituite anche da componenti automatiche.
Tale entità quindi, come del resto gran parte delle precedenti, deve essere realizzata per offrire facile riutilizzabilità anche in
progetti esterni a questa architettura.



\section {Considerazioni finali}

Nel corso di questo capitolo si sono trattati alcuni delle problematiche fondamentali
riscontrabili nel raggiungimento di un'adeguata spiegabilità dei sistemi di apprendimento automatico,
cercando di trovarvi soluzione in un'architettura applicativa. Il tentativo è stato quello di
identificare un modello capace di fornire un'adeguata separazione concettuale di tutte le parti
coinvolte al fine di ospitare realizzazioni varie e versatili, che permettano l'aggiunta di
componenti innovative qualora se ne presentino in futuro. Di fatto non sono perciò presenti
linee guida circa l'interpretabilità e la spiegabilità dei modelli di apprendimento, le quali
sono totalmente affidate alle implementazioni specifiche dell'architettura formalizzata.

Gli approcci implementativi per il modello proposto possono essere molteplici. Il caso più semplice
potrebbe consistere in una realizzazione monolitica autocontenuta studiata per funzionare su
un'unico dispositivo, scelta che vincolerebbe tuttavia i processi lavorativi alle singole caratteristiche
del contesto d'esecuzione della macchina ospitante. Nel prossimo capitolo
si affronterà un caso di studio che tratta la realizzazione di un'applicativo software che segue
questo principio, semplicemente finalizzato a dimostrare le fattibilità dei concetti discussi e
a fornire una base di codice riutilizzibile in progetti più complessi.
Nell'epilogo di questa tesi verranno invece discusse possibili metodologie realizzative più
ambiziose, che permetterebbero un'espressione migliore delle potenzialità del concetto architettuale
qui trattato.

Va infine messo in luce che questo tipo di soluzioni non sono di certo sufficienti per ottenere
il rapporto di qualità tra macchine ed esseri umani che si desidera raggiungere, ma si limitano
a donare un mezzo infrastrutturale su cui basare le tecnologie applicative coinvolte.
In primo luogo le difficoltà più determinanti si riscontrano infatti nell'interpretazione dei
modelli di apprendimento automatico ai quali si è più interessati.
Non si dispone ancora purtroppo dei mezzi teorici necessari per un'opportuna comprensibilità
delle strutture di previsione più prestanti, limitando drasticamente le loro potenziali applicazioni
nonostante esse riescano a dimostrare risultati spesso promettenti. In aggiunta a ciò va ricordato
che l'individuo medio della popolazione mondiale non prova ancora reale fiducia nei confronti dei
sistemi digitali. Agli occhi di molti il rapido espandersi delle realtà software e dei sistemi
pervasivi risulta alle volte invasivo e minaccioso, dando importanza a quanto questi differiscano
dal genere di servizi presenti in precedenza e portando in secondo piano le effettive opportunità
che essi sarebbero in grado di offrire. Nel mondo odierno si assiste quindi ad un'adattamento
delle persone ai canoni delle strutture software presenti, in continuo mutamento
e parte attiva della vita lavorativa e personale. Specialmente in vista delle sempre più reali
capacità proattive dei servizi in questione, sarebbe invece ideale riuscire a dotare gli stessi
della capacità di adattarsi ai propri utilizzatori, rispettandone necessità e desideri,
presentando loro costrutti adeguati alle loro capacità, volontà, privacy.
Ritengo quindi che il problema della ``spiegabilità'' sia radicato nella sua natura tecnica informatica
ma sconfini abbondantemente in tematiche sociali, etiche, e culturali da affrontare con attenzione e
cautela ma al contempo necessarie per raggiungere lo stato di benessere e produttività per cui
tutti i sistemi software sono, alla base, stati effetivamente in principio ideati.










