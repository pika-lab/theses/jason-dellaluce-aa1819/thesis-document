\chapter {Valutazione di alcuni framework di Machine Learning}
\label{cap3}

La preparazione di questa tesi prevede sotto numerosi aspetti una ricognizione e comprensione
delle tecnologie di Machine Learning, di come i modelli coinvolti siano strutturati, di quali siano
i flussi di lavoro previsti.
Nel capitolo precedente sono stati opportunamente introdotti alcuni concetti teorici a fondamento
della disciplina. Nelle seguenti sezioni saranno invece analizzati alcuni tra gli strumenti di lavoro
protagonisti del settore, affinché sia possibile valutare correttamente alcune scelte realizzative
nella parte sperimentale della tesi.



\section{Anaconda} 

Anaconda\cite{anaconda-page} è una distribuzione libera ed open-source sotto licenza BSD\cite{bsd-page}
dei linguaggi di programmazione Python e R, inizialmente rilasciata nel 2012, il cui scopo è facilitare
la gestione dei pacchetti ed il deployment. Questo strumento è tra i più utilizzati nel suo genere
comprendendo oltre 1500 pacchetti per la computazione scentifica e quindi Data Science, Machine Learning,
Data-Processing di larga scala, analisi statistica e predittiva. La gestione delle componenti installate,
delle dipendenze e delle versioni è affidata ad un motore interno chiamato \textit{Conda} che si sostituisce
al più famoso \textit{pip} tipico degli ambienti Python\cite{python-page}, il quale è ugualmente
utilizzabile se preferito.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth] {Images/LogoAnaconda}
	\caption{Logo della distribuzione Anaconda}
\end{figure}

Il pacchetto su cui questa tesi pone il suo concreto interesse è sicuramente
\textbf{SciKit-Learn}\cite{sklearn-article}. Inizialmente progettato durante l'evento
\textit{Google Summer of Code}\cite{google-soc-page} del 2007, questa libreria open-source comprende
una voluminosa collezione di implementazioni, modelli, algoritmi per il Data Mining ed il Machine
Learning basandosi su framework di calcolo noti in ambiente Python come NumPy, SciPy e matplotlib.
Per mantenere un supporto multi-piattaforma consistente, le scelte progettuali hanno previsto di escludere
ottimizzazioni hardware o GPU che avrebbero aumentato il peso delle dipendenze e delle specificità
architetturali, lasciando quindi tutti i calcoli a carico della CPU.

SciKit-Learn contiene numerose implementazioni di algoritmi di Machine Learning noti, dei quali il seguente
elenco si limiterà a riassumere in linea generale i modelli teorici coinvolti illustrandone rapidamente
i principi:

\begin{description}	
	\item[Modelli Lineari] |
		Una volta codificati tutti gli attributi in forma numerica (discreta o continua che sia)
		queste idee prevedono di utilizzare tecniche matematiche di separazione o
		regressione lineare per riprodurre gli output desiderati.
		Essendo ben pochi i problemi di natura lineare, queste metodologie si estendono ai
		casi non lineari alterando il dominio delle variabili a polinomi di grado più alto
		(Spazio Z\cite{book-lfdata}), misura da applicare con cautela in quanto facile portatrice di
		Overfitting. Oltre a Classificazione Lineare e Regressione Lineare che si occupano
		rispettivamente di problemi numerici discreti e continui, esiste un terzo caso chiamato
		Regressione Logistica che si occupa di problemi con output numerico continuo
		compreso tra 0 e 1, estendendo quindi i limiti discreti delle classificazioni.
		Tali tecniche fanno uso di funzioni matematiche facilmente differenziabili, che
		permettono minimizzazioni d’errore di alto livello come Gradient Descent.

	\item[Alberi Decisionali e Strutture ad Albero] |
		Questo genere di strutture sono già state
		opportunamente approfondite nel capitolo precedente.
	
	\item[Regole Decisionali, Liste Decisionali] |
		La struttura di un albero decisionale può essere codificata come un insieme ordinato
		di regole logiche, sotto forma di CNF (Conjunctive Normal Form\cite{book-aima}). In questo caso,
		oltre ad una garantita maggior leggibilità, si può anche godere di classificazioni con
		casi di “default”, imitando la struttura ad \textit{if-else} dei linguaggi imperativi.
		Per via della necessità di una relazione d’ordine tra le regole, questi modelli
		possono essere anche codificati in strutture a lista.
	
	\item[Reti Neurali Artificiali] |
		Queste metodologie tentano di riprodurre la struttura del nostro cervello, creando un
		grafo pesato di cui i nodi sono chiamati “neuroni”.
		Solitamente si organizzano serie di neuroni non collegati tra loro, detti livelli; ogni
		neurone di un livello è poi collegato con un certo peso a tutti i neuroni dei livelli
		precedente e successivo (Feed-Forward Network).
		La catena è iniziata da un livello di neuroni dedicato agli attributi input, e conclusa da
		uno dedicato agli output. Il comportamento di ogni neurone è definito da una relazione matematica detta \textit{Funzione di Attivazione}, la quale è alle volte un Regressore Logistico.
		Si parla solitamente di Deep Learning\cite{book-aima} quando la rete costruita è composta
		da numerosi livelli e risulta particolarmente complessa.
		Oltre ad essere considerevolmente versatili (anche per applicazioni di Reinforcement Learning),
		queste tecniche risultano particolarmente prestanti nei problemi di natura non lineare.
		Le Reti Neurali sono trattate superficialmente in SciKit-Learn, e necessiterebbero di
		ottimizzazioni hardware per via della complessità delle computazioni necessarie.
		
	\item[Nearest Neighbor] |
		Dato un input, questi modelli prevedono di cercare un determinato numero di
		esempi simili tra quelli già visti per poi dedurre da questi una categoria o farne
		una regressione. La “vicinanza” tra esempi è data da una misura di distanza
		multidimensionale, che spesso è la Distanza Euclidea classica.
		Con input dai numerosi attributi il calcolo della distanza fra esempi diventa
		costosa: questo problema è detto \textit{Dimensionality Curse}\cite{book-aima}.
	
	\item[Support Vector Machines] |
		In questa tecnica si cerca tra i dati un numero esempi di “supporto” che
		facciano da limitatori per una separazione lineare ottimale.
		In tutti i casi in cui la linearità non è possibile, si applica una trasformazione ai
		dati che aumenti la dimensionalità degli stessi (Feature Space\cite{book-aima}) andando a
		cercare quindi una classificazione lineare in dimensioni superiori a quelle
		iniziali.
		
	\item[Modelli Probabilistici] |
		Esistono una moltitudine di tecniche che prevedono l’utilizzo di metodi classici della
		statistica al fine di apprendere un modello probabilistico che descriva i dati. Il che può
		significare stimare la distribuzione di probabilità dei dati campione, eseguire
		un’analisi probabilistica finalizzata alla classificazione, oppure fare clustering.
		Queste metodologie sono inoltre facilmente abbinabili a molte delle altre tecniche di
		Machine Learning per migliorarne l’efficienza.
		
	\item[Ensemble Methods] |
		Questa metodologia prevede di utilizzare un insieme di modelli ipotetici per
		riuscire a descrivere i dati, a differenza dei casi precedenti che affinavano sempre una singola ipotesi.
		Ogni modello può poi essere pesato rispetto agli altri in base alle necessità.
		Di fatto è una possibilità di potenziamento per le altre tecniche.
\end{description}

La libreria fornisce inoltre numerosi strumenti per la trasformazione dei dati, la valutazione delle performance
degli algoritmi, il processo di Model Selection, la persistenza dei modelli addestrati.
Vi è anche supporto per varie tecniche di Unsupervised Learning, che non verranno tuttavia approfondite in quanto
distanti dagli obiettivi di questa tesi.
Risultano invece assenti strumenti adeguati per i problemi di Reinforcement Learning.



\section{Keras e TensorFlow}

Keras\cite{keras-page} è una libreria open-source disponibile sotto licenza MIT\cite{mit-page} scritta
in Python per utilizzi avanzati dei modelli di apprendimento automatico a Rete Neurale Artificiale.
Essa è stata progettata per permettere sperimentazioni semplificate e veloci mantenendo una struttura
modulare, estendibile e user-friendly.
Sono contenute numerose implementazioni delle componenti più comuni per la costruzione di Reti Neurali profonde
come livelli, obiettivi, funzioni di attivazione, ottimizzatori. In aggiunta, sono supportati anche modelli
più avanzati che prevedono componenti di rete convoluzionali e ricorrenti.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth] {Images/LogoKerasTensorflow}
	\caption{Logo di Keras (a sinistra) e di TensorFlow (a destra)}
\end{figure}

Lo scopo principale del progetto è rendere più agibile la costruzione di modelli che sarebbero per loro natura
ricchi di tecnicismi, e proprio per questo l'architettura è stata pensata come un'interfaccia di programmazione
di alto livello.
Infatti, Keras non si presenta come una libreria stand-alone e non contiene implementazioni che si occupino
delle computazioni reali richieste nelle fasi di apprendimento. Si rendono invece necessari software esterni
che fungano da back-end di calcolo per rendere completa l'operatività del software, il quale si limita ad esporre
moduli che permettano la costruzione di Reti Neurali in maniera totalmente indipendente dall'implementazione scelta.
In questo senso, i back-end ufficialmente supportati sono \textbf{Theano}\cite{theano-page}, \textbf{CNTK}
(Microsoft Cognitive Toolkit)\cite{cntk-page}, e \textbf{TensorFlow}.

TensorFlow\cite{tf-page} è una libreria open-source sviluppata dal \textit{Google Brain Team} e pubblicata sotto
licenza Apache 2.0\cite{apache2-page} nel 2015. Si tratta di uno strumento di computazione numerica basato su
grafi, dei quali ogni nodo contiene un operazione matematica, ed ogni arco rappresenta un vettore multi-dimensionale
meglio identificato con il termine \textbf{Tensore}.
Questa struttura relativamente flessibile permette di eseguire agevolmente computazioni complesse su uno o più
desktop, server, o dispositivi mobili con ottimizzazioni per CPU e GPU senza riscrivere codice. TensorFlow espone
inoltre API native per Python, C, C++, Java, Go, JavaScript, e Swift mantenendo supporto ad un ampia gamma di sistemi
operativi e dispositivi hardware.
Questo software è ad oggi tra le soluzioni più adottate per svolgere i calcoli necessari a Machine Learning e
Deep Learning, seppur queste non siano le uniche applicazioni possibili.

L'utilizzo di TensorFlow come back-end per Keras risulta essere una scelta relativamente popolare nel settore.
La necessità di librerie specializzate sui modelli a Rete Neurale è dovuta alla complessità riscrontrabile durante
le fasi di addestramento. Infatti, seppur particolarmente efficienti e robuste, queste strutture di apprendimento
automatico necessitano di numerosi tentativi ed enormi quantità di dati per affinare la precisione dei legami interni
tra i neuroni, e di conseguenza considerevoli quantità di tempo. Considerando inoltre che in numerose applicazioni
le reti che si intende costruire risultano particolarmente fitte (Deep Learning), si rende quindi necessario introdurre
ottimizzazioni di calcolo ovunque possibile.

La natura dei due software trattati in questa sezione è sostanzialmente diversa da quella degli altri framework analizzati nel resto del capitolo, proprio per via della specificità dei casi d'uso.
Tuttavia è giusto evidenziare che Keras offre interfacce wrapper che rendono possibile l'utilizzo dei suoi modelli
all'interno dei flussi di lavoro di SciKit-Learn.



\section{Weka}

Weka\cite{weka-page} è una collezione software di strumenti per l'apprendimento automatico 
sviluppata nell'Università di Waikato in Nuova Zelanda.
Il nome, acronimo di Waikato Environment for Knowledge Analysis, trae ispirazione
da una specie locale di uccello inadatta al volo particolarmente cara
alla popolazione neozelandese.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.4\textwidth] {Images/LogoWeka}
	\caption{Logo del framework Weka}
\end{figure}
Si tratta di un framework open-source interamente sviluppato in linguaggio Java 
pubblicato con licenza GNU General Public License\cite{gnu-gpl-page}, la quale 
concede la distribuzione libera ma non un usufrutto commericiale.
Le applicazioni accessibili all'utente di cui questo software dispone, sono:
\begin{itemize}
	\item \textbf{Explorer} | Un ambiente autocontenuto adatto all'esplorazione
	dei dati e all'applicazione degli algoritmi di apprendimento.
	
	\item \textbf{Experimenter} | Un ambiente nel quale condurre esperimenti e misure statistiche
	relativi agli schemi di apprendimento offerti.
	
	\item \textbf{KnowledgeFlow} | Un ambiente particolarmente simile ad Explorer,
	ma che aggiunge un interfaccia grafica più ricca con aventuali funzioni drag-and-drop.
	
	\item \textbf{Workbench} | Un applicazione all-in-one che permette di interagire con tutti
	gli ambienti di lavoro sopra citati.
	
	\item \textbf{SimpleCLI} | Un interfaccia a riga di comando per interagire
	a basso livello con le componenti del software in tutti i sistemi operativi
	che non espongono una propria command-line.
\end{itemize}
Oltre ad essere un applicativo stand-alone Weka permette di lavorare con tutte le proprie componenti
attraverso API Java che la rendono a tutti gli effetti una libreria\cite{weka-manual}, facilmente
integrabile in progetti esterni che intendano svolgere mansioni di Machine Learning e Data Mining.
Questa tesi pone il suo concreto interesse su questa ultima funzionalità.
Weka vanta una collezione di implementazioni ed algoritmi di apprendimento automatico particolarmente
ricca e comparabile a quella di SciKit-Learn, già vista in precedenza.
Avendo infatti già analizzato la natura dei modelli teorici coinvolti, e per via della forte tipizzazione
presente in Java, per questa libreria ha senso approfondire le classi fondamentali che delineano
interfacce e strutture alla base di tutti gli algoritmi implementati.

Il pacchetto \textbf{weka.core} comprende tutte le classi base necessarie al funzionamento
dell'intero sistema.
Le componenti fondamentali sulle quali è necessario porre particolare attenzione sono quelle
che descrivono la struttura dei dati, sulle quali tutti i modelli di apprendimento andranno 
a lavorare.

\begin{description}
	\item[Attribute] | Un oggetto di questa classe immutabile rappresenta un attributo.
	É fondamentale precisare che la funzione di questa componente è puramente descrittiva,
	andando quindi a esplicitare nome, tipo e valori possibili di un determinato attributo ma
	non il valore ad esso associato in una specifica istanza.
	Un attributo rappresentabile con questa classe può essere di tipo numerico, nominale, testuale,
	relazionale o relativo ad una data. La scelta di design attuata è stata quella di rappresentare
	tutte le diverse tipologie di dato in questa singola classe, evitando invece gerarchie di oggetti
	più complesse che andassero a specificare ogni singolo caso.
	
	\item[Instance] | Un oggetto di questa classe rappresenta una particolare istanza di un
	Data Set. La componente si presenta come una collezione ordinata di \textbf{Attribute} e dei
	loro rispettivi valori associati. Ogni valore relativo ad un attributo è rappresentato internamente
	come un numero reale, andando ad evitare quindi un approccio ad oggetti più sofisticato per
	favorire le performance del sistema\cite{weka-manual}.
	In caso un attributo sia di tipo nominale, testuale o relazionale, il numero reale rappresenta
	una quantità intera corrispondente all'indice nella lista dei valori possibili per quello specifico
	dato.
	
	\item[Instances] | Un oggetto di questa classe rappresenta un Data Set, inteso come collezione
	ordinata e pesata di \textbf{Instance}. Questa struttura dati può essere letta da file di diversi
	formati attraverso alcune componenti \textbf{Loader} contenute in altri package, tuttavia può essere
	anche costruita a piacimento aggiungendo istanze in maniera incrementale. In quanto descrittività
	degli attributi contenuti, sono state mantenute le stesse scelte di design della classe Instance.
	Inoltre l'attributo ``classe'', oggetto della previsione di un modello di apprendimento, è da esplicitare
	in questa componente mediante il metodo \texttt{setClassIndex}.
\end{description}

Il pacchetto \textbf{weka.classifiers} colleziona tutte le implementazioni degli algoritmi di apprendimento
automatico supervisionato per la classificazione e la regressione numerica.
Le classi chiave, alla base di tutti i modelli supportati, sono:

\begin{description}
	\item[Classifier] | Gli oggetti di questa classe rappresentano un determinato modello
	di apprendimento insieme all'algoritmo che è in grado di generarlo. La scelta progettuale ha
	previsto che questa struttura debba essere la base sia di dei modelli di tipo classificativo
	che di quelli di natura regressiva. Un \textbf{Classifier} espone i tre metodi fondamentali
	\texttt{buildClassifier}, \texttt{classifyInstance} e \texttt{distributionForInstance}:
	il primo si occupa di generare il modello di previsione allenando l'algoritmo interno su uno
	specifico Data Set \textbf{Instances}, mentre il secondo ed il terzo permettono di fare previsioni
	su una nuova istanza.
	L'output delle previsioni di questa struttura è generalmente un numero reale, che in caso di
	regressione rappresenta la quantità continua attesa, mentre per i modelli classificativi indica
	l'indice intero relativo alla lista di tutti i valori possibili. Questa scelta semplicistica
	é presumibilmente nuovamente dovuta alla ricerca di buone performance, a discapito di una buona
	struttura orientata agli oggetti\cite{weka-javadoc}.
	
	\item[Evaluation] | Un oggetto di questa classe consente di verificare l'efficacia di un
	modello di apprendimento misurando l'accuratezza delle sue previsioni su un Data Set di test
	attreverso il metodo \texttt{evaluateModel}. Sono disponibili anche metodi per le tecniche di
	Cross Validation\cite{book-lfdata} e per indagini statistiche di varia natura al fine di una
	più completa comprensione della performance del modello in esame.
\end{description}

La rappresentazione dei campioni di dati e la relativa compatibilità con le diverse fonti
dalle quali questi provengono sono fattori in cui Weka ha prestato particolare attenzione.
Nel pacchetto \textbf{weka.core.converters} sono collezionate varie implementazioni che permettono
di interfacciare la libreria a diversi formati rappresentativi di Data Set, dei quali sono citati
solo quelli più importanti:

\begin{description}
	\item[CSV] | Acronimo di \textit{Comma-Separated values}\cite{book-lfdata} è un formato di
	file testuale adatto alla rappresentazione di tabelle. Non esiste una sintassi formale che ne
	definisca la struttura, tuttavia è generalmente previsto che ogni riga della tabella rappresentata
	corrisponda ad una riga nel file di testo, che a sua volta viene divisa da un carattere
	separatore (tipicamente, appunto, la virgola) in sottoparti che corrispondono alle colonne.
	Risulta essere un formato comodo per contenere un Data Set, dove ogni riga corrisponde 
	ad un'istanza di esempio e ogni colonna è relativa ad un attributo.
	In questo modo, in ogni cella è contenuto il valore di un determinato attributo per una specifica
	istanza. Tipicamente è anche previsto che la prima riga sia impiegata ad esplicitare il nome degli
	attributi stessi, imponendo inoltre la suddivisione in colonne iniziale.
	La classe \textbf{CSVLoader} implementa un lettore di questo genere di file.
	
	\item[ARFF] | Acronimo di \textit{Attribute-Relation File Format}\cite{book-lfdata} è un formato
	di file testuale introdotto dalla libreria stessa.
	La novità peculiare di questa struttura è la possibilità di rappresentare istanze con
	attributi di tipo relazionale, ossia direttamente provenienti da una stesura dei record
	di un database relazionale.
	Oltre ad essere uno standard rappresentativo di file, questo formato è anche utilizzato di default
	come tassonomia testuale interna della classe Instances\cite{weka-manual}.	
\end{description}

Anche in questa libreria sono presenti, seppur non approfonditi, classi e strumenti per Feature Selection,
Model Selection, persistenza, algoritmi di Unsupervised Learning.
Come in SciKit-Learn non è tuttavia implementato un supporto per Reinforcement Learning e più in generale
tecniche di Deep Learning.



\section{Analisi conclusiva}

Esistono numerosi altri framework e librerie non approfondite in questa tesi, tuttavia è possibile concludere
che l'infrastruttura Python è sicuramente la preferita per le applicazioni di Machine Learning e Data Mining.
Di fatto non sembra esserci alcuna qualità tecnica specifica che induca gli sviluppatori a creare strumenti
in tale linguaggio, se non la semplicità di utilizzo.
Ciò nonostante l'ecosistema Python si è negli ultimi anni arricchito di numerosi software per questi specifici
domini applicativi, e si sta affermando come linguaggio principale utilizzato per il calcolo.
Le performance tuttavia non sono particolarmente favorite, e a differenza di altre piattaforme queste librerie
devono utilizzare intensamente componenti scritte in C e C++ per una miglior efficienza delle computazioni
complesse.

SciKit-Learn è inoltre uno dei pochi strumenti ad affermarsi come libreria multi-purpouse per il Machine Learning
in quanto la maggior varietà di software sembra essere sviluppato per il più specifico settore del Deep Learning,
focalizzato esclusivamente sui modelli a Rete Neurale, nel quale Keras sembra affermarsi come scelta ormai standard.
TensorFlow gode invece di particolare popolarità per via dell'attivo mantenimento e la ricca documentazione, ciò
nonostante esistono alternative valide ed alle volte più efficienti in termini di velocità di esecuzione come Caffe o CNTK.
Ne si può concludere che i casi d'uso dei framework Python analizzati in questo capitolo sono di fatto diversi,
il che rende tali strumenti complementari e particolarmente adatti ad essere integrati l'uno con l'altro.

Seppur meno considerata vi sono ottime ragioni anche per la creazione di librerie di Machine Learning in linguaggio
Java.
Esso e la Java Virtual Machine sono infatti ancora tra le basi più utilizzate per le applicazioni Enterprise ed
i progetti di medio-larga scala, garantendo supporto ad una considerevole quantità di software ed aziende che
trarrebbero beneficio ad espandersi nel dominio dell'apprendimento automatico su una piattaforma già nota.
Inoltre le performance di calcolo della JVM sono tipicamente superiori a quelle dell'ambiente Python, rendendola
sotto questo aspetto un'opzione preferibile. Va citato peraltro che tale piattaforma sia nativa del linguaggio Scala,
il quale è considerato da diversi esperti un ottimo candidato come standard futuro per il calcolo scientifico.

Una libreria degna di nota scritta in Java è Deeplearning4j, anch'essa particolarmente popolare per le applicazioni
del Deep Learning. Weka invece, esaminata in questo capitolo, soffre di un'utenza decrescente e di ormai poca
popolarità dovute generalmente alla scarsa efficienza delle implementazioni, alla poca scalabilità e al carente
mantenimento del progetto. Tuttavia essa rimane una delle poche librerie multi-purpose sotto i vari aspetti
del Machine Learning disponibili in Java, godendo inoltre di diverse innovazioni introdotte in passato e note nella
letteratura. Approfondirne i contenuti può essere quindi un'esperienza formativa molto valida.
Essa inoltre presenta una struttura semplice e di facile intelligibilità, rendendola adatta a prototipi e
sperimentazioni.
Come verrà esposto in seguito, Weka è infatti risultata essere una scelta comoda per la parte sperimentale di
questa tesi.

