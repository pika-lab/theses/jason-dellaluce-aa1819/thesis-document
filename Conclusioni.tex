\chapter {Considerazioni e sviluppi futuri}

Questa tesi è stata primariamente incentrata sulla ricognizione delle caratteristiche della tematica \textbf{Explainable AI}, cercando nello specifico di categorizzarne le problematiche principali e di trovarvi soluzione in una possibile architettura concettuale su cui basare realizzazioni future di agenti intelligenti spiegabili.
In secondo luogo è stata posta l'attenzione sullo sviluppo di un prototipo implementativo di tale architettura, il quale ha avuto lo scopo di dimostrare la fattibilità dei concetti, mettere in luce necessità e vincoli della fase realizzativa, proporre alcune formalizzazioni per le strutture coinvolte, e fornire una base di codice semplice su cui basare sviluppi più sofisticati.
Nello specifico, il prototipo è stato focalizzato sulla costruzione di spiegazioni narrative partendo dalle tecniche di Machine Learning.
Tale obiettivo è stato raggiunto traducendo la struttura computazionale di un modello predittivo in conoscienza logica, la quale è stata successivamente alla base di inferenze logiche orientate al riconoscimento di soluzioni per le interrogazioni degli utenti.
Le narrazioni sono infine state generate seguendo linearmente i percorsi logici costruiti negli alberi di prova delle dimostrazioni.
Su questa linea di principio questa tesi concepisce una visione più grande, che coinvolge la realizzazione di un framework consistente in cui tecniche simboliche e sub-simboliche possano essere combinabili con coerenza per lo sviluppo di applicazioni intelligenti, strumentalizzando al meglio i pregi dei due diversi approcci.


\section{Considerazioni sulla proposta architetturale}

Lo studio condotto ha permesso di identificate alcuni problemi principali che ostacolano la comprensibilità dei sistemi intelligenti capaci di apprendimento automatico.
A tal riguardo, l'archittettura concettuale concepita in questa tesi propone l'adozione della Logica del Primo ordine come linguaggio formale per la codifica delle strutture decisionali automatiche e delle inferenze da esse prodotte, e presenta al contempo adeguate astrazione e modularità che permettono l'integrazione di soluzioni future.
Ciò è stato possibile esplicitando un basilare modello di interazione studiato per separare concettualmente le diverse entità coinvolte.

Come già appurato il modello in questione può essere alla base di svariate idee di realizzazione di cui il prototipo sviluppato nella tesi rappresenta un semplice esempio dimostrativo, nel cui caso è stato scelto di costruire il progetto su una struttura monolitica studiata per lavorare su una singola macchina ospitante.
Nonostante questo tipo di vincolo non costringa lo sviluppo ad un singolo linguaggio di programmazione, esso presenta numerose limitazioni tecniche e funzionali.
In primo luogo un siffatto software è fortemente vincolato al contesto di esecuzione della macchina su cui è installato, inteso come l'insieme di capacità computazionale, memoria, informazioni contestuali su cui basare l'apprendimento.
Secondariamente è possibile riconoscere limitazioni sulla comunicazione instaurabile tra le varie entità del modello, le quali sarebbero costrette ad interagire proceduralmente nella stessa infrastruttura o mediante interazioni inter-processo.
Una modellazione monolitica non rispecchia perciò al loro meglio i concetti di modularità e scalabilità caratteristici della proposta architetturale.

Seppur distante dagli obiettivi della tesi si intende qui esporre la realizzazione ritenuta ideale per una massima espressione delle qualità del modello, ossia quella di un \textbf{servizio distribuito} su una rete di macchine comunicanti.
In questa progettazione ideale ogni entità dell'architettura sarebbe ospitata ed eseguita da una o più macchine diverse, e le interazioni con le stesse avverrebbero mediante richieste remote.
Tale scelta garantirebbe alta riutilizzabilità, facile sostituibilità, implementabilità eterogenea e differenziata per ognuna delle componenti coinvolte. L'intero sistema sarebbe inoltre influenzato da contesti di esecuzione ed informazioni contestuali di tutti i dispositivi partecipanti alla rete, portando alla costruzione di una macroscopica intelligenza virtuale che gode delle inferenze di tutti gli agenti distribuiti nello spazio, rispecchiando i principi del paradigma pervasivo.
\begin{figure}[!ht]
	\centering
	\makebox[\linewidth]{\includegraphics[width=1.0\linewidth]{Images/FutureDev1}}
	\caption{Architettura concettuale rivista in un'ottica distribuita}
	\label{futuredev-1}
\end{figure}

A tal proposito, in Figura \ref{futuredev-1} è rappresentata una possibile rivisitazione dell'architettura concettuale proposta in questa tesi seguendo l'idea di un servizio distribuito.
In essa è stato riconosciuto che \textbf{Logic Programming as a Service} implementa quasi alla perfezione l'evoluzione nell'ottica distribuita delle entità Knowledge Base e Demonstrazion, con l'unica lacuna delle proprietà di dimostrazione le quali potrebbero tuttavia essere aggiunte considerando un'estensione del software o incapsulandolo per realizzare un servizio più specifico.
Si potrebbe perciò allo stesso modo pensare ad un'evoluzione dell'entità Machine Learning Interface in un \textbf{servizio distribuito per il Machine Learning}, capace di ospitare e riprodurre tutti i flussi di lavoro ed i comportamenti di un modello di apprendimento automatico supervisionato.
Questa nuova componente è pensata come un'insieme di istanze software distribuite nella rete, ognuna delle quali deve rispettare un'interfaccia di comunicazione standardizzata e predefinita in cui però l'implementazione, i framework sottostanti, e i sistemi di persistenza possono essere realizzati in maniera totalmente indipendente.
\begin{figure}[!ht]
	\centering
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{Images/FutureDev2}}
	\caption{Esempio di interfaccia di comunicazione unificata nel servizio}
	\label{futuredev-2}
\end{figure}
Ogni istanza deve essere configurabile da un'amministratore per designare un determinato algoritmo di apprendimento, una delle disponibili realizzazioni sottostanti, sintassi e grammatiche da rispettare per le comunicazioni di interfaccia.
Ogni interazione con l'istanza dovrebbe inoltre essere preferibilmente \textit{stateless} per agevolare la scomposizione del carico lavorativo.
Infine l'interfaccia deve prevedere l'esposizione dei descrittori degli attributi dei dati accettati, metodi per l'aggiunta incrementale o collettiva delle istanze di dato, richieste di interrogazione per ottenere classificazioni (e regressioni) di nuovi esempi, ed infine un sistema di serializzazione che permetta di esportare la struttura decisionale allenata sottostante.
Periodi di addestramento, sistemi di persistenza e privacy dei dati rimarrebbero canoni arbitrari dipendenti dalle singole implementazioni.
È opportuno mettere in luce che secondo quest'ottica l'aggiunta incrementale dei dati, e quindi la costruzione del Data Set di allenamento, non sarebbe più a carico dell'ente ospitante ma bensì ad una porzione degli utenti stessi del servizio.
Implementando quindi opportuni sistemi di certificazione, tracciabilità, e protezione questo modello si rivelerebbe perfettamente compatibile con i dispositivi IoT, i quali procurerebbero con responsabilità autonoma grandi flussi di dati provenienti dall'interazione con le persone.

Tornando all'architettura generale della Figura \ref{futuredev-1}, si potrebbe concludere che Logic Programming as a Service e l'appena presentato servizio di Machine Learning assumono un ruolo ``passivo'', mostrandosi come servizi indipendenti, interrogabili, e sfruttabili in svariati domini applicativi.
Le entità Explanation e Theorization potrebbero rappresentare a tal punto agenti ``attivi'' esterni, capaci di interrogare uno dei due servizi, elaborare le sue strutture, e proporle all'altro nei termini a lui consoni.
Sviluppi futuri di questa tesi, o altri progetti sensibili ad Explainable AI, potrebbero pertanto considerare questo modello come linea guida concettuale.



\section{Considerazioni sul prototipo}

Il collaudo del prototipo ha messo in evidenza quanto esso rispetti a pieno tutte le specifiche imposte dal progetto, e che pertanto gli obiettivi esemplificativi di questa tesi sono stati effettivamente soddisfatti.
Infatti i modelli di apprendimento di Weka sono stati integrati con successo, i predicati logici generati dalla spiegazione degli Alberi Decisionali non hanno registrato perdite informative, e i testi in linguaggio naturale prodotti dalle implementazioni dimostrative godono di sufficiente leggibilità. 
È invece opportuno criticare, e semmai rivalutare, alcune delle ipotesi progettuali circa il modello logico delle componenti del software e il formato di alcune strutture in esse coinvolte, proponendo occasionalmente possibili estensioni del progetto stesso.

In primo luogo sarebbe importante abolire la violazione dell'incapsulamento delle entità Machine Learning Interface.
L'introduzione di codifiche e grammatiche standard per la serializzazione di tutti i possibili modelli di apprendimento sarebbe un'operazione particolarmente costosa, pertanto andrebbero considerate soluzioni alternative come l'implementazione del design pattern Visitor nell'interfaccia IClassifier. 
In tal modo ogni struttura Machine Learning potrebbe rendersi esternamente esplorabile in modo specifico per ogni singola realizzazione, e la spiegazione in predicati logici potrebbe avvenire in una nuova gerarchia di classi \textbf{Explainer} da accettare come visitatori.
Questo genere di cambiamento favorirebbe inoltre la scalabilità del progetto e la possibilità di mantenere diverse realizzazioni di spiegazione per la stessa struttura.

Una seconda fondamentale miglioria sarebbe l'indebolimento della dipendenza del software dalla libreria Weka, riducendo il suo ruolo a quello di un ``backend'' per gli algoritmi di apprendimento.
Per raggiungere tale obiettivo sarebbe necessario introdurre una gerarchia di classi interna per la rappresentazione delle istanze di dato, degli attributi, e dei Data Set.
Applicare una tassonomia interna e condivisa procurerebbe un decremento delle prestazioni dovuto alla conversione della stessa nei formati utilizzati dai singoli framework di apprendimento automatico integrati, ma supererebbe tuttavia gran parte dei vincoli imposti da Weka.

Si potrebbe inoltre considerare l'introduzione di componenti che permettano la misurazione del grado di errore commesso dai modelli di apprendimento automatico nelle loro previsioni. Sebbene ogni framework Machine Learning preveda strutture proprietarie per questo tipo di mansione, il progetto dovrebbe adottare costrutti standard da utilizzare su generiche entità Machine Learning Interface. Ciò nonostante, mediante opportuni sistemi di interfacce ed adattatori, alcune librerie potrebbero ugualmente essere integrate come ``backend'' per facilitare la realizzazione di questi costrutti.

Un'altra tematica non esplicitamente toccata dal progetto è quella della configurabilità e tuning dei modelli di apprendimento, che costituiscono una fase fondamentale nei processi lavorativi della Model Selection.
Sarebbe a tal proposito opportuno introdurre un sistema unificato di configurazione, al quale ogni singola realizzazione di Machine Learning Interface dovrebbe adattarsi per ottenere i relativi parametri, oppure studiare una gerarchia di classi \textbf{Configurator} da estendere per ogni realizzazione specifica.

Rimane inoltre discutibile anche il formato dei predicati logici ottenuti dal processo di spiegazione degli Alberi Decisionali.
Nell'implementazione attuale vengono esplorati tutti i percorsi che collegano la radice dell'albero con i suoi nodi foglia, portando alla generazione di predicati logici disgiunti la cui condizione di verità risulta semanticamente analoga alla congiunzione di tutti gli \textit{split} dei nodi attraversati.
Nonostante la notazione scelta sia effettivamente corretta va reso presente che essa non è di certo l'unica possibile e, come le sperimentazioni sulle entità Demonstration hanno messo in luce, le regole così prodotte possono non godere di particolare leggibilità specialmente in relazione ad un'inferenza logica.
Una possibile futura alternativa sarebbe quella di codificare l'Albero Decisionale in un'insieme di predicati ricorsivi, tali che la medesima struttura decisionale compaia negli Alberi di Prova generati durante il raggiungimento delle soluzioni.

Rimanendo sulla tematica delle componenti Demonstration, nel capitolo precedente è stato messo in luce che l'attuale notazione dei predicati logici nei testi in linguaggio naturale può essere un fattore molto limitante nei termini della leggibilità.
Nonostante risulti semplice utilizzare occasionalmente una notazione infissa per i predicati di arità pari a due, è invece complesso generalizzare una codifica comune adatta a tutto il resto delle casistiche.
Si può concludere che sarebbe più opportuno introdurre sistemi di configurabilità per le classi Demonstrator, in modo da rendere possibile personalizzazioni sofisticate di volta in volta adatte ai singoli domini applicativi e casi d'uso. \\
Infine sarebbe coerente introdurre tecniche alternative al testo in linguaggio naturale per le dimostrazioni, tra cui le più intuitive potrebbero sicuramente essere reppresentazioni grafiche da integrare in interfacce utente dell'applicativo.

Andrebbe successivamente anche ampliato il supporto a framework di Machine Learning alternativi a Weka, in modo da integrare implementazioni più varie di modelli ed algoritmi di apprendimento.
La scelta del framework Weka nel prototipo è stata inizialmente motivata dall'analisi qualitativa discussa nel Capitolo \ref{cap3}, che ha evidenziato l'effettiva facilità di utilizzo della libreria.
Considerando poi la scelta di realizzare il progetto in linguaggio Java, l'utilizzo di Weka ha permesso di lavorare su una sola infrastruttura e di evitare \textit{bindings} software con ambienti esterni.
Implementare, per esempio, la compatibilità con i framework scritti in Python avrebbe comportato la realizzazione di mezzi di comunicazione con la macchina virtuale Java, presumibilmente mediante interazioni inter-processo.

Infine si considera utile sviluppare un opportuno livello di interfacce utente, in modo da racchiudere le funzionalità del framework prototipato in un applicativo utilizzabile in sistemi a finestre o mediante riga di comando.


